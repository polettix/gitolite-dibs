#!/bin/sh

exec >&2
set -eux

username="$1"
groupname="$2"
appdir="$3"
homedir="$4"
srcdir="$(cat DIBS_DIR_SRC)"

cp -a "$srcdir" "$appdir"
for compressed in "$appdir"/*.bz2 ; do
   [ -e "$compressed" ] || continue
   bunzip2 "$compressed"
done
chown -R "$username:$groupname" "$appdir"

mkdir -p "$homedir"
chown -R "$username:$groupname" "$homedir"

cd /var/lib
rm -rf nginx
ln -s /app/gitweb/nginx
