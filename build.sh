#!/bin/sh
set -eu
md="$(dirname "$(readlink -f "$0")")"
cd "$md"
if ! [ -d "src/gitolite" ] ; then
   (
      cd src
      git clone https://github.com/polettix/gitolite.git gitolite
   )
fi
dibs -A "$@"
