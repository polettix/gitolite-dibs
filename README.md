# Gitolite-Dibs

This repository helps wrapping [Gitolite][] in a [Docker][] image that aims to
be also easily deployable with [Kubernetes][].

## Getting the image

The image is available in the [container registry][].

There is some minimal help by just running it, with ample space for improvement:

```
docker run --rm registry.gitlab.com/polettix/gitolite-dibs
```

## Configurations

Configuring the behaviour from outside can be done by means of ad-hoc
mounts and/or environment variables.

### SSHD configuration

Environment variable `SSHD_CONFIG_DIR` can be added to point to a
directory where the booting configurations and host keys can be placed.
It's possible for this location to be mounted from an external
directory/mount.

If not provided, a directory will be created in `$HOME/ssh` and
populated with a `sshd_config` file shipped with the image (if not
already present), as well as host keys generated automatically if not
present.

Updates to the configuration file `sshd_config` require re-creating the
container to take effect.

Updates to the host keys *might* be considered, depending on whether the
`sshd` daemon caches them internally or not. In doubt, it's better to
re-create the container.

### Administrator (`admin`) public key

User `admin` serves as a catchall administrator and its key can be
provied/updated by means of environment variable `ADMIN_PUBLIC_KEY`.

This variable is read only upon startup of the container, any later
change is ignored and updates require re-creation of the container.

### Configuration file `.gitolite.rc`

File `~/.gitolite.rc` is used by [Gitolite][] to store general
configurations (e.g. active modules). It is formatted as a valid
[Perl][] source code file, where configurations are stored in hash
`%RC`.

[Gitolite][] reads this file every time it is run, which can happen
multiple times in the lifetime of the container. For this reason, it is
possible to update it without requiring a restart of the container.

By default, it is placed at `/home/git/.gitolite.rc`.

If environment variable `GITOLITE_RC_PATH` is set, the current file is
removed and substituted with a symbolic link to that path. If the file
does not exist, an error is raised.

## Helm Chart

The sub-directory `helm` contains the needed parts to build a [Helm][]
chart. Run script `pack.sh` inside to generate a tarball that is
suitable for running `helm`.

The chart is still in a bit of flux. The biggest pain point is managing
the [PersistentVolume][] and the deployment might shift to a
[StatefulSet][] in the future.

All said, it's immediately useable with the following configuration
options in the `values.yaml` file:

- `image`: the usual stuff here, like a `name` that points to the image
  (most probably in a registry) and a `pullPolicy` that does what you
  think.
- `service`: allows setting the details for the service. [Gitolite][] is
  accessible through SSH in this image, so the port is 22 by default.
  It's possible to bind to a specific `node_port` if `type` is set as
  such.
- `volume`: this is the volume where repositories are kept, which is
  also user `git`'s home directory. It's supposed to be a filesystem.
  Available keys are `access_mode` (defaulting to `ReadWriteMany`),
  `size` (defaulting to 10 Gi) and the `storage_class` (defaulting to
  the empty string). It's also possible to enable a section about `nfs`,
  which currently does nothing special apart generate the specification
  for a [PersistentVolume][] automatically in the printed text at the
  end of a deployment via `helm`.
- `config`: this is where [Gitolite][]-specific or less specific stuff
  ends up. It has several sub-sections:
  - `admin_public_key`: the key to associate to the `admin` user, which
    is the first user created and also enabled to do remote management
    through the `gitolite-admin` repository;
  - `host_keys`: there are three sub-keys pointing to the respective
    keys, namely `rsa`, `ecdsa`, and `ed25519`;
  - `sshd_config`: the file `sshd_config` itself;
  - `gitolite_rc`: what will be used as file `~/.gitolite.rc`.

Configurations end up in [ConfigMaps][] (`admin_public_key` and
`gitolite_rc`) and a [Secret][] (`sshd_config` and `host_keys`) for
later tweaking, although it's best to only use `helm` with an updated
local `values.yaml` file at this point.

At the end of the deployment, a file is printed including the YAML for a
[PersistentVolume][], which can be used to create the `git` home volume
separately. Also the management of handing over the same volume to a
different installation is something that has to be addressed manually at
the moment (upgrades in the chart should work fine though).

## Hacking

Make sure you have [dibs][].

Clone this repository and run `build.sh` - it will make sure to also clone
[this fork of the gitolite repository][fork]. It's possible to fork from the
original repository of course.

## Copyright and License

This repository aims at packaging [Gitolite][].

The [Gitolite][] software is not included in this repository but is
packaged inside the final [Docker][] image (including the images stored
in the [container registry][].

### Gitolite

The [Gitolite][] software is copyright Sitaram Chamarty and is licensed
under the GPL v2; please see the file called COPYING in the source
distribution.

Please see [http://gitolite.com/gitolite/#license][gitolite-license] for
more.

> NOTE: GIT is a trademark of Software Freedom Conservancy and their use
> of "Gitolite" is under license.

### Pandoc

This repository includes a statically compiled version of [Pandoc][]:

> Pandoc is free software, released under the [GPL][]. Copyright 2006–2021
> [John MacFarlane][].

### This repository

Copyright 2022 by Flavio Poletti (flavio `AT` polettix.it).

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0][apache-2.0].

or look for file `LICENSE` in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[Gitolite]: https://gitolite.com/gitolite/
[Docker]: https://www.docker.com/
[Kubernetes]: https://kubernetes.io/
[container registry]: https://gitlab.com/polettix/gitolite-dibs/container_registry
[dibs]: https://github.com/polettix/dibs
[fork]: https://github.com/polettix/gitolite
[gitolite-license]: http://gitolite.com/gitolite/#license
[apache-2.0]: http://www.apache.org/licenses/LICENSE-2.0
[Helm]: https://helm.sh/
[PersistentVolume]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[StatefulSet]: https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/
[ConfigMaps]: https://kubernetes.io/docs/concepts/configuration/configmap/
[Secret]: https://kubernetes.io/docs/concepts/configuration/secret/
[Pandoc]: https://pandoc.org/
[GPL]: http://www.gnu.org/copyleft/gpl.html
[John MacFarlane]: https://johnmacfarlane.net/
