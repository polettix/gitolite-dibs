#!/bin/sh
cd "$(dirname "$(readlink -f "$0")")"
version="$(readlink ../VERSION)"
dir="gitolite-dibs-$version-chart"
rm -rf "$dir"
mkdir -p "$dir"
tar cC gitolite-dibs --exclude='*.sw*' . | tar xC "$dir"
sed -i -e "s/__VERSION__/$version/g" "$dir/values.yaml"
sed -i -e "s/__VERSION__/$version/g" "$dir/Chart.yaml"
tar czf "$dir.tar.gz" "$dir"
rm -rf "$dir"

mkdir -p ../src/helm
tar cjf ../src/helm-chart.tar.bz2 "$dir.tar.gz"
